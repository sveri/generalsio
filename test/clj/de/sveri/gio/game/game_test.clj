(ns de.sveri.gio.game.game-test
  (:require [clojure.test :refer :all]
            [de.sveri.gio.game.graph :as g]
            [puget.printer :as pp]))


;(def graph [{:army 80, :terrain 0, :idx 0, :city true}
;            {:army 294, :terrain 1, :idx 1, :city true}
;            {:army 11, :terrain 1, :idx 2, :city true}
;            {:army 296, :terrain -4, :idx 3, :city true}
;            {:army 7, :terrain -1, :idx 4}
;            {:army 38, :terrain -1, :idx 5}
;            {:army 3, :terrain -4, :idx 6}
;            {:army 6, :terrain -4, :idx 7, :city false}
;            {:army 6, :terrain -1, :idx 8, :city false}])
;
;
;(def graph-with-mount [{:army 80, :terrain 0, :idx 0, :city true}
;                       {:army 294, :terrain -2, :idx 1, :city true}
;                       {:army 11, :terrain -1, :idx 2, :city true}
;                       {:army 296, :terrain -4, :idx 3, :city true}
;                       {:army 7, :terrain -1, :idx 4}
;                       {:army 38, :terrain -1, :idx 5}
;                       {:army 3, :terrain -4, :idx 6}
;                       {:army 6, :terrain -4, :idx 7, :city false}
;                       {:army 6, :terrain -1, :idx 8, :city false}])





(def graph-for-weighted-test [{:army 80, :terrain 0, :idx 0}
                              {:army 294, :terrain -2, :idx 1,}
                              {:army 11, :terrain -1, :idx 2}
                              {:army 296, :terrain -1, :idx 3}
                              {:army 7, :terrain -1, :idx 4}
                              {:army 50, :terrain -1, :idx 5, :city true}
                              {:army 50, :terrain -1, :idx 6, :city true}
                              {:army 3, :terrain -1, :idx 7}
                              {:army 6, :terrain -1, :idx 8}
                              {:army 6, :terrain 1, :idx 9}
                              {:army 6, :terrain -1, :idx 10}
                              {:army 6, :terrain -1, :idx 11}
                              {:army 6, :terrain -1, :idx 12}
                              {:army 6, :terrain -1, :idx 13}
                              {:army 6, :terrain -1, :idx 14}
                              {:army 6, :terrain -1, :idx 15}])

(def width 4)
(def height 4)

(deftest next-move-for-from-to-tiles-test
  (let [move (g/next-move-for-from-to-tiles {:army 6, :terrain 1, :idx 9} [{:army 6, :terrain -1, :idx 10}
                                                                           {:army 6, :terrain -1, :idx 11}
                                                                           {:army 6, :terrain -1, :idx 12}]
                                            (g/generate-graph {:player-index 11 :width 4 :height 4} (:distance g/weights) graph-for-weighted-test))]
    (is (= [{:army 6, :idx 9, :terrain 1} {:army 6, :idx 10, :terrain -1}] move))))

(deftest next-move-for-from-tiles-to-tile-test
  (let [move (g/next-move-for-from-tiles-to-tile [{:army 6, :terrain -1, :idx 10}
                                                  {:army 6, :terrain -1, :idx 11}
                                                  {:army 6, :terrain -1, :idx 12}]
                                                 {:army 6, :terrain 1, :idx 9}
                                                 (g/generate-graph {:player-index 11 :width 4 :height 4} (:distance g/weights) graph-for-weighted-test) {})]
    (is (= [{:army 6, :idx 10, :terrain -1} {:army 6, :idx 9, :terrain 1} ] move))))


(deftest is-any-of-to-tiles-in-distance-of-from-tile?
  (let [res (g/is-any-of-to-tiles-in-max-distance-of-from-tile?
              {:army 6, :terrain 1, :idx 9} [{:army 6, :terrain -1, :idx 15}]
              1
              (g/generate-graph {:player-index 11 :width 4 :height 4} (:distance g/weights) graph-for-weighted-test))]
    (is (= false res)))
  (let [res (g/is-any-of-to-tiles-in-max-distance-of-from-tile?
              {:army 6, :terrain 1, :idx 9} [{:army 6, :terrain -1, :idx 15}]
              10
              (g/generate-graph {:player-index 11 :width 4 :height 4} (:distance g/weights) graph-for-weighted-test))]
    (is (= true res))))



;(deftest get-direct-neighbors
;  (let [neigbhors (g/get-direct-neighbor-maps graph {:army 80, :terrain 0, :idx 0, :city true} graph-width 3)]
;    (is (= [{:army 294, :terrain 1, :idx 1, :city true} {:army 296, :terrain -4, :idx 3, :city true}]
;           neigbhors)))
;  (let [neigbhors (g/get-direct-neighbor-maps graph {:army 294, :terrain 1, :idx 1, :city true} graph-width 3)]
;    (is (= [{:army 80, :terrain 0, :idx 0, :city true} {:army 11, :terrain 1, :idx 2, :city true} {:army 7, :terrain -1, :idx 4}]
;           neigbhors)))
;  (let [neigbhors (g/get-direct-neighbor-maps graph {:army 11, :terrain 1, :idx 2, :city true} graph-width 3)]
;    (is (= [{:army 294, :terrain 1, :idx 1, :city true} {:army 38, :terrain -1, :idx 5}]
;           neigbhors)))
;  (let [neigbhors (g/get-direct-neighbor-maps graph {:army 296, :terrain -4, :idx 3, :city true} graph-width 3)]
;    (is (= [{:army 7, :terrain -1, :idx 4} {:army 3, :terrain -4, :idx 6} {:army 80, :terrain 0, :idx 0, :city true}]
;           neigbhors)))
;  (let [neigbhors (g/get-direct-neighbor-maps graph {:army 6, :terrain -1, :idx 8, :city false} graph-width 3)]
;    (is (= [{:army 6, :terrain -4, :idx 7, :city false} {:army 38, :terrain -1, :idx 5}]
;           neigbhors))))
;
;(deftest remove-mountains
;  (is (= [{:army 11, :terrain -1, :idx 2, :city true}]
;        (g/remove-mountains [{:army 294, :terrain -2, :idx 1, :city true} {:army 11, :terrain -1, :idx 2, :city true}]))))
;
;(deftest get-path
;  (let [path (g/get-path-from-to graph-with-mount {:army 80, :terrain 0, :idx 0, :city true} {:army 11, :terrain -1, :idx 2, :city true} graph-width 3 0)]
;    (is (= [{:army 80, :terrain 0, :idx 0, :city true} {:army 296, :terrain -4, :idx 3, :city true} {:army 7, :terrain -1, :idx 4}
;            {:army 38, :terrain -1, :idx 5} {:army 11, :terrain -1, :idx 2, :city true}]
;           path))))
;
;(deftest test-weights
;  (let [path (g/get-path-from-to graph-for-weighted-test {:army 6, :terrain -1, :idx 9} {:army 11, :terrain -1, :idx 2} 4 4 0)]
;    (is (= [{:army 6, :terrain -1, :idx 9} {:army 6, :terrain -1, :idx 10} {:army 6, :terrain -1, :idx 11}
;            {:army 3, :terrain -1, :idx 7} {:army 296, :terrain -1, :idx 3} {:army 11, :terrain -1, :idx 2}]
;           path))))



;(def wt {:0 {:1 1 :2 100 :3 1}
;         :1 {:0 1 :2 10}
;         :2 {:3 10}})

