(ns generalsio.dev
  (:require [schema.core :as s]
            [de.sveri.gio.core :as core]))

(s/set-fn-validation! true)

(enable-console-print!)

(defn main [] (core/main))
