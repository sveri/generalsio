(ns de.sveri.gio.controls)

(def player-id-to-color-map
  {-5 "white"
   -4 "white"
   -3 "white"
   -2 "white"
   -1 "white"
   0 "lightgreen"
   1 "#FFC300"
   2 "#aebff1"
   3 "yellow"
   4 "#8e44ad"
   5 "#99a3a4"
   6 "#d7bde2"
   7 "#aed6f1"})

(defn draw-score-col [player-id score]
  [:span (str (when (= player-id (:i score)) "ME: ") "total: " (:total score) " tiles: " (:tiles score))])

(defn draw-scores [player-id scores]
  (let [rest-players (filter #(not= player-id (:i %)) scores)]
    [:div.table
     [:div.tr
      (for [score scores]
        ^{:key (str "score-" (:player-index score))} [:div.td (draw-score-col player-id score)])]]))


(defn game-controls [{:keys [send-fn]} width height player-names replay-id player-id]
  [:div
   [:button.btn.btn-primary {:style {:margin-right 20}
                             :on-click #(send-fn [:game.core/start-game])} "start-game"]
   [:button.btn.btn-primary {:style {:margin-right 20}
                             :on-click #(send-fn [:game.core/stop-game])} "stop-game"]
   [:button.btn.btn-primary {:style {:margin-right 20}
                             :on-click #(send-fn [:game.core/join-1v1])} "Join 1v1"]
   [:button.btn.btn-primary {:style {:margin-right 20}
                             :on-click #(send-fn [:game.core/join-ffa])} "Join FFA"]
   [:button.btn.btn-primary {:style {:margin-right 20}
                             :on-click #(send-fn [:game.core/autojoin-on])} "Autojoin on"]
   [:button.btn.btn-primary {:style {:margin-right 20}
                             :on-click #(send-fn [:game.core/autojoin-off])} "Autojoin off"]
   [:button.btn.btn-primary {:style    {:margin-right 20}
                             :on-click #(send-fn [:game.core/stars-and-rank])} "Rank"]
   [:span {:style {:margin-right 20}} (str width "x" height)]
   [:span {:style {:margin-right 20}} (str "Players: " player-names)]
   [:a {:style {:margin-right 20} :target "_blank" :href (str "http://bot.generals.io/replays/" replay-id)} (str "http://bot.generals.io/replays/" replay-id)]
   [:br]
   [:br]
   [:br]])


