(ns de.sveri.gio.game.graph
  (:require [loom.graph :as g]
            [loom.io :as lio]
            [loom.alg :as la]
            [cuerdas.core]
            [clojure.spec :as s]
            [de.sveri.gio.game.helper :refer [MOUNTAIN] :as h]))


(s/def ::city boolean?)
(s/def ::idx (s/and int? #(<= 0 %) #(< % 1000)))
(s/def ::terrain (s/and int? #(<= -4 %) #(< % 10)))
(s/def ::army (s/and int? #(<= 0 %) #(< % 10000)))

(s/def ::context-map (s/keys :req-un [::army ::terrain ::idx] :opt-un [::city]))

(s/def ::context-maps (s/and (s/coll-of ::context-map) #(= (count %) (-> (map :idx %) distinct count))))



(def non-moveable-tiles (atom #{}))


(def weights {:distance {:my-tile (fn [_] 1)
                         :enemy-tile (fn [_] 1)
                         :empty-tile (fn [_] 1)
                         :city-tile (fn [_] 1)}

              :default {:my-tile #(+ 0 (if (= 0 (:army %)) 0 (/ 1 (:army %))))
                        :enemy-tile #(:army %)
                        :empty-tile (fn [_] 1)
                        :city-tile #(:army %)}

              :defend-general {:my-tile (fn [_] 1)
                               :enemy-tile (fn [_] 1)
                               :empty-tile (fn [_] 1)
                               :city-tile #(:army %)}
              :gather-for-my-general {:my-tile #(:army %)
                                      :enemy-tile #(:army %)
                                      :empty-tile (fn [_] 1)
                                      :city-tile #(:army %)}})
              ;:gather-for-my-general {:my-tile #(/ 1 (:army %))
              ;                        :enemy-tile #(:army %)
              ;                        :empty-tile (fn [_] 1)
              ;                        :city-tile #(:army %)}})


(defn get-weight-for-map [player-index weight-map context-map]
  (cond
    (h/is-enemy? player-index context-map) ((:enemy-tile weight-map) context-map)
    (h/is-me? player-index context-map) ((:my-tile weight-map) context-map)
    (h/is-empty? context-map) ((:empty-tile weight-map) context-map)
    (h/is-city? context-map) ((:city-tile weight-map) context-map)))

(defn get-weight-for-maps [player-index weight-map context-maps]
  (mapv
    #(get-weight-for-map player-index weight-map %)
    context-maps))

(defn exclude-map-from-path? [context-map can-take-city?]
  (or (h/is-mountain? context-map)
      (and (if (h/is-city? context-map) (not can-take-city?) false))))

(defn generate-graph [{:keys [player-index can-take-city?] :as game-info} weight-map context-maps]
  (g/weighted-graph
    (reduce
     (fn [weighted-graph context-map]
       ;(if (or (h/is-mountain? context-map) (h/is-city? context-map)))
       (if (exclude-map-from-path? context-map can-take-city?)
       ;(if (or (h/is-mountain? context-map))
         weighted-graph
         (let [direct-neigbhors (-> (h/get-direct-neighbors context-map game-info context-maps) h/remove-mountains h/remove-cities)]
           ;(let [direct-neigbhors (-> (h/get-direct-neighbors context-map game-info context-maps) remove-mountains remove-cities)])
           (if (empty? direct-neigbhors)
             weighted-graph
             (assoc weighted-graph context-map (zipmap direct-neigbhors (get-weight-for-maps player-index weight-map direct-neigbhors)))))))
     {}
     context-maps)))

(defn- get-paths-from-to-tiles-with-distance [from-tile to-tiles graph]
  (mapv #(la/dijkstra-path-dist graph from-tile %) to-tiles))

(defn- get-paths-from-tiles-to-with-distance [from-tiles to-tile graph]
  (mapv #(la/dijkstra-path-dist graph % to-tile) from-tiles))

(defn get-path-from-to [tile-from tile-to {:keys [default-graph]} context-maps]
  (vec (la/dijkstra-path default-graph tile-from tile-to)))



; opts {:descending boolean}
(defn- next-move-for-paths [paths opts]
  (let [paths-wo-nil (remove nil? paths)]
    (if (empty? paths-wo-nil)
      []
      (let [sorted-path (sort-by second paths-wo-nil)
            sorted-path (if (get opts :descending false) (reverse sorted-path) sorted-path)
            first-path (-> sorted-path ffirst)
            first-path (if (= 1 (count first-path)) (-> (sort-by second paths-wo-nil) second first) first-path)]
        (if (<= 2 (count first-path))
          [(first first-path) (second first-path)]
          [])))))

(defn next-move-for-from-to-tiles [from-tile to-tiles graph]
  (next-move-for-paths (get-paths-from-to-tiles-with-distance from-tile to-tiles graph) {}))

; opts {:descending boolean}
(defn next-move-for-from-tiles-to-tile [from-tiles to-tile graph opts]
  (next-move-for-paths (get-paths-from-tiles-to-with-distance from-tiles to-tile graph) opts))



(defn is-any-of-to-tiles-in-max-distance-of-from-tile? [from-tile to-tiles max-distance graph]
  (as-> (get-paths-from-to-tiles-with-distance from-tile to-tiles graph) paths
        (remove nil? paths)
        (sort-by second paths)
        (<= (-> paths first second) max-distance)))


(defn get-tiles-within-distance [{:keys [distance-graph]} from-tile max-distance context-maps]
  (as-> (get-paths-from-to-tiles-with-distance from-tile context-maps distance-graph) paths
        (remove nil? paths)
        (sort-by second paths)
        (filter #(< (-> paths first second) max-distance) paths)))
