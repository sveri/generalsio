(ns de.sveri.gio.game.helper
  (:require [clojure.tools.logging :as log]
            [com.rpl.specter :as spect]))

(def PLAYER_INDEXES 0)
(def EMPTY -1)
(def MOUNTAIN -2)
(def FOG -3)
(def FOG_OBSTACLE -4)                                  ;// Cities and Mountains show up as Obstacles in the fog of war.


;(def states [:base-building :enemy-in-sight :general-under-attack :attack-enemy-general])

(defn subvec-to-end [v from to]
  (if (empty? v)
    v
    (try
      (vec
        (if (<= to (count v))
         (subvec (vec v) from to)
         (subvec (vec v) from)))
      (catch Exception e (log/error "tried to subvec: " v " - " from " - " to)))))




(defn is-me? [tile player-index] (= player-index (:terrain tile)))

(defn is-empty? [tile] (= EMPTY (:terrain tile)))

(defn is-city? [tile] (contains? tile :city))

(defn is-mountain? [tile] (= MOUNTAIN (:terrain tile)))

(defn is-my-general? [tile player-index] (and (is-me? tile player-index) (contains? tile :general)))

(defn get-my-general [game-info] (:my-general game-info))

(defn get-own-tile-with-largest-army [{:keys [player-index my-tiles] :as game-info} min-army-size context-maps]
  (as-> (filter #(<= min-army-size (:army %)) my-tiles) tiles
        (vec tiles)
        (sort-by :army tiles)
        (reverse tiles)
        (first tiles)))


;(defn get-n-largest-own-tiles [player-index min-size n context-maps]
;  (subvec-to-end (filter #(and (<= min-size (:army %)) (is-me? % player-index)) context-maps) 0 n))


(defn get-own-tiles-with-min-size [player-index min-size context-maps]
  (vec (filter #(and (<= min-size (:army %)) (is-me? % player-index)) context-maps)))

(defn get-own-tiles [player-index context-maps] (get-own-tiles-with-min-size player-index 0 context-maps))


(defn get-avg-armies-per-tile [context-maps]
  (/ (reduce (fn [sum context-map] (+ sum (:army context-map))) 0 context-maps) (count context-maps)))

(defn get-own-avg-armies [player-index context-maps]
  (get-avg-armies-per-tile (get-own-tiles-with-min-size player-index 0 context-maps)))


; indexes = [1 34]
(defn get-context-maps-for-indexes [indexes context-maps]
  (mapv #(nth context-maps %) indexes))





;(s/fdef get-direct-neighbor-maps :args (s/cat :context-maps ::context-maps :from-tile ::context-map :width int? :height int?))
(defn get-direct-neighbors [from-tile {:keys [width height]} context-maps]
  (if-let [idx (:idx from-tile)]
    (let [x-neighbors (cond
                        (= (dec width) (mod idx width)) [(dec idx)]
                        (= 0 (mod idx width)) [(inc idx)]
                        :else [(dec idx) (inc idx)])
          neigbhors (concat x-neighbors [(+ idx width) (- idx width)])
          neighbors-in-map (vec (filter #(and (<= 0 %) (< % (* width height))) neigbhors))]
      (get-context-maps-for-indexes neighbors-in-map context-maps))
    []))


(defn get-empty-non-city-neighbors [tile game-info context-maps]
  (let [neighbors (get-direct-neighbors tile game-info context-maps)]
    (filter #(and (not (is-city? %)) (not (is-mountain? %)) (is-empty? %)) neighbors)))

(defn get-my-outer-tiles [{:keys [player-index my-tiles] :as game-info} context-maps]
  (filter #(< 0 (count (get-empty-non-city-neighbors % game-info context-maps))) my-tiles))




;;;;;;;;;; enemies

(defn is-enemy? [player-index context-map]
  (let [tile (:terrain context-map)]
    (and (not= player-index tile) (<= PLAYER_INDEXES tile))))

(defn get-enemies [player-index context-maps]
  (filter #(is-enemy? player-index %) context-maps))


(defn has-enemies? [player-index context-maps]
  (not-empty (get-enemies player-index context-maps)))


;(defn can-conquer-next-tile? [from-tile to-tile player-index]
;  (or (is-me? to-tile player-index)
;      (< (:army to-tile) (dec (:army from-tile)))))



;;;;;;;;;;;;; cities


(defn- get-cities [neutral? player-index context-maps]
  (let [cities (filter #(contains? % :city) context-maps)]
    (if neutral?
      (filter #(= EMPTY (:terrain %)) cities)
      (filter #(and (<= PLAYER_INDEXES (:terrain %)) (not= (:terrain %) player-index)) cities))))

(defn get-neutral-cities [player-index context-maps] (get-cities true player-index context-maps))

(defn get-neutral-city-with-lowest-army [player-index context-maps] (first (sort-by :army (get-neutral-cities player-index context-maps))))

(defn get-own-cities [player-index context-maps]
  (filter #(and (contains? % :city) (= player-index (:terrain %))) context-maps))



(defn remove-cities [context-maps]
  (vec (remove #(or (nil? %) (is-city? %)) context-maps)))



(defn remove-mountains [context-maps]
  (vec (remove #(or (nil? %) (is-mountain? %)) context-maps)))


;(defn can-enter-city? [from-tile to-tile player-index context-maps]
;  (or (not (is-city? to-tile))
;      (and (is-city? to-tile) (is-me? to-tile player-index))
;      (and (is-city? to-tile) (can-conquer-next-tile? from-tile to-tile player-index))))
;
;
;(defn should-collect-for-city? [from-tile player-index context-maps]
;  (let [lowest-city (get-neutral-city-with-lowest-army player-index context-maps)]
;    (and from-tile
;         lowest-city
;         (< 40 (count (filter #(is-me? % player-index) context-maps)))
;         (< (:army from-tile) (:army lowest-city))
;         (< (count (get-own-cities player-index context-maps)) 1)
;         (not (has-enemies? player-index context-maps)))))
;
;
;
;
;
;
;(defn remove-non-moveable-tiles [non-moveable-tiles context-maps]
;  (reduce
;    (fn [acc context-map]
;      (if (some #{context-map} non-moveable-tiles)
;        acc
;        (conj acc context-map)))
;    []
;    context-maps))
;
;
;
;(defn remove-first-path-from-paths [current-paths]
;  (when (< 0 (count @current-paths)) (reset! current-paths (subvec @current-paths 1))))
;
(defn get-empty-tiles [context-maps] (vec (filter #(= EMPTY (:terrain %)) context-maps)))



