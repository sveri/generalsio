(ns de.sveri.gio.game.core
  (:require
    [clojure.data.json :as json]
    [cuerdas.core :as str]
    [de.sveri.gio.game.logic :as log]
    [de.sveri.gio.game.ws-helper :as wsh]
    [de.sveri.gio.game.helper :as h])
  (:import (io.socket.client IO Socket Ack)
           (io.socket.emitter Emitter$Listener)
           (java.net URLEncoder)
           (org.json JSONObject)))

(declare start-game join-ffa join-1v1)

(def user_id "sveri_bot_id12309isdfklj")
(def user_name "[Bot] sveri")

(def custom_game_id "custom_sveri_game")

(def ws-connections (atom {}))

;(def ws-url "ws://botws.generals.io/socket.io/?EIO=3&transport=websocket")
(def generalsio-ws-url "http://botws.generals.io")

(def auto-join (atom false))


(defn leave-game [s ws]
  (.emit s "leave_game" (into-array [])))


(defn stop [ws]
  (let [socket (:first-socket @ws-connections)]
    (println "disconnecting socket")
    (leave-game socket ws)
    (.disconnect socket)
    (when @auto-join
      (Thread/sleep 15000)
      (join-1v1 ws))))


;
;{"teams" [1 2],
; "replay_id" "HeIjEPUde",
; "usernames" ["Anonymous" "Anonymous"],
; "playerIndex" 0,
; "chat_room" "game_1486415005680-7-SzpjbTZB7VRr3ABQV"}
(defn game-started [data socket send-fn connected-uids]
  (let [game-info (-> data first .toString (json/read-str :key-fn keyword))]
    (alter-var-root #'log/player-index (constantly (:playerIndex game-info)))
    (alter-var-root #'log/game-information (constantly game-info))
    (println game-info)
    (println (format "http://bot.generals.io/replays/%s" (:replay_id game-info)))
    (wsh/send-game-started send-fn connected-uids game-info)))


(defn rank-star-received [data rank-or-star]
  (println rank-or-star ": " (-> data first .toString (json/read-str :key-fn keyword))))

(defn connect [{:keys [send-fn connected-uids] :as ws}]
  ;(stop)
  (let [socket (IO/socket generalsio-ws-url)
        on-connect (reify Emitter$Listener
                     (call [this args] (println "connected")))
        on-disconnect (reify Emitter$Listener
                        (call [this args] (println "disconnected: " args)))]
    (.on socket Socket/EVENT_CONNECT on-connect)
    (.on socket Socket/EVENT_DISCONNECT on-disconnect)
    (.on socket Socket/EVENT_ERROR (reify Emitter$Listener
                                     (call [this args] (rank-star-received args "error: "))))
    (.on socket "game_start" (reify Emitter$Listener
                               (call [this data]
                                 (println "game_start received")
                                 (game-started data socket send-fn connected-uids))))
    (.on socket "game_update" (reify Emitter$Listener
                                (call [this data] (log/handle-update data socket send-fn connected-uids))))
    (.on socket "game_won" (reify Emitter$Listener
                             (call [this data] (println "game won")
                               (stop ws))))
    (.on socket "game_lost" (reify Emitter$Listener
                              (call [this data] (println "game lost")
                                (stop ws))))
    (.on socket "error_set_username" (reify Emitter$Listener
                                       (call [this data] (println "set username: "
                                                                  (-> data .toString)))))
    (.on socket "stars" (reify Emitter$Listener
                          (call [this data] (rank-star-received data "star"))))
    (.on socket "rank" (reify Emitter$Listener
                         (call [this data] (rank-star-received data "rank"))))
    (swap! ws-connections assoc :first-socket socket)
    (.connect socket)))

;(defn start-game [socket])
(defn start-game [ws]
  (connect ws)
  (Thread/sleep 2000)
  (let [socket (:first-socket @ws-connections)]
  ;(let [socket (connect ws)]
    (.emit socket "join_private" (into-array [custom_game_id, user_id]))
    (.emit socket "set_force_start" (into-array [custom_game_id, "true"]))
    (println (str/format "joined game at: http://bot.generals.io/games/%s" custom_game_id))))


(defn join-1v1 [ws]
  (connect ws)
  (Thread/sleep 2000)
  (let [socket (:first-socket @ws-connections)]
    (.emit socket "set_username" (into-array [user_id user_name]))
    ;(.emit socket "play" (into-array [user_id]))
    (.emit socket "join_1v1" (into-array [user_id]))
    (println (str/format "joined 1v1"))))

(defn join-ffa [ws]
  (connect ws)
  (Thread/sleep 2000)
  (let [socket (:first-socket @ws-connections)]
    (.emit socket "set_username" (into-array [user_id user_name]))
    (.emit socket "play" (into-array [user_id]))
    (println (str/format "joined FFA"))))

(defn ask-for-stars-and-rank []
  (let [socket (:first-socket @ws-connections)]
    (.emit socket "stars_and_rank" (into-array [user_id]))))

(defn autojoin [on-off]
  (reset! auto-join on-off))


;(defn conn-and-start [socket]
;  (connect)
;  (Thread/sleep 2000)
;  (start-game socket))