(ns de.sveri.gio.game.gathering
  (:require [de.sveri.gio.game.graph :as g]
            [de.sveri.gio.game.helper :as h]))


(defn get-from-to-move [{:keys [distance-graph player-index my-general] :as game-info} from-distance context-maps]
  (let [g (g/generate-graph game-info (:gather-for-my-general g/weights) context-maps)
        own-tiles-in-distance (g/get-tiles-within-distance game-info my-general 7 context-maps)]
    ;(println own-tiles-in-distance)
    (println (type (vec (own-tiles-in-distance 1))))
        ;own-tiles-in-distance (filter h/is-me? (g/get-tiles-within-distance game-info my-general 7 context-maps))]
    (g/next-move-for-from-tiles-to-tile (h/get-own-tiles-with-min-size player-index 2 context-maps) my-general g {:descending true})))
