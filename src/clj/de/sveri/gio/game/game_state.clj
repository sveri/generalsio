(ns de.sveri.gio.game.game-state
  (:require [clojure.spec :as s]
            [de.sveri.gio.game.graph :as g]
            [de.sveri.gio.game.helper :as h]))


;(s/def ::expansion :expansion)


;(s/def ::states #{::expansion})
(s/def ::states #{:base-building :expansion :find-enemy-general :defend-my-general? :enemy-in-sight :enemy-took-city :take-city
                  :has-enemy-generals})





(defn enemy-close-to-my-general? [{:keys [player-index my-general has-enemies? distance-graph] :as game-info} context-maps]
  (and has-enemies?
    (g/is-any-of-to-tiles-in-max-distance-of-from-tile? my-general has-enemies? 6 distance-graph)))


(defn min-army-to-attack [] 15000)

(def attacking-tile (atom nil))

(defn set-attacking-tile [{:keys [has-enemies? has-enemy-generals? turn my-general] :as game-info} context-maps]
  (cond
    (enemy-close-to-my-general? game-info context-maps) (reset! attacking-tile nil)

    (and (not (nil? @attacking-tile)) (< (:army @attacking-tile) 3)) (reset! attacking-tile nil)

    (and has-enemies?
         (nil? @attacking-tile)
         (< (min-army-to-attack) (:army (h/get-own-tile-with-largest-army game-info 2 context-maps))))
    (reset! attacking-tile (h/get-own-tile-with-largest-army game-info 2 context-maps))))





(s/fdef get-game-state :ret ::states)
(defn get-game-state [{:keys [has-enemies? has-enemy-generals? turn my-general] :as game-info} context-maps]
  (set-attacking-tile game-info context-maps)
  ;(println @attacking-tile)
  (cond
    (enemy-close-to-my-general? game-info context-maps) :defend-my-general
    (and has-enemies? (not-empty (h/get-my-outer-tiles game-info context-maps)) (< (mod turn 50) 20)) :base-building
    has-enemy-generals? :has-enemy-generals
    (and has-enemies? (nil? @attacking-tile)) :gather-for-my-general
    ;(< (h/get-own-tile-with-largest-army game-info 2 context-maps) (min-army-to-attack)) :gather-for-my-general
    ;(and (enemy-close-to-my-general? game-info context-maps) (< (mod turn 50) 15) (< (:army my-general) 140)) :gather-for-my-general
    ;(and has-enemies? (< 25 (mod turn 50))) :attack-enemy
    (and has-enemies? (not (nil? @attacking-tile))) :attack-enemy
    (not-empty (h/get-my-outer-tiles game-info context-maps)) :base-building
    :else :attack-enemy)
  :gather-for-my-general)
